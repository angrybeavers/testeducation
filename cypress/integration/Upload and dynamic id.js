import 'cypress-file-upload';

describe('Upload files and create task', () => {
  before(function () {
    cy.testlogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
  })

  beforeEach(function () {
    Cypress.Cookies.preserveOnce('idsrv.session')
    cy.clearLocalStorage()
  })

  it('Navigate to Task tab', () => {
    cy.server()
    cy.route('GET', 'api/orders/tasks/presets').as('tasks')

    cy.get('[href="/sanatop/private/tasks"] > .q-btn__wrapper').click()
    
    cy.get('.text-h5').should('contain', 'Задачи')
    cy.wait('@tasks').then((response) => {
      expect(response.status).to.eq(200)
    })
  })


  it('Find task', () => {
    cy.get('.q-mr-sm > .q-field__inner > .q-field__control > .q-field__append > .q-select__dropdown-icon').click() //выбор статуса
    cy.get(':nth-child(1) > .q-item__section > .q-item__label').click()
    cy.get('.q-col-gutter-sm > :nth-child(2) > .q-field > .q-field__inner > .q-field__control').type('01.08.2020') //дата создания
    cy.get('.q-col-gutter-sm > :nth-child(3)').type('13.10.2020')
    cy.get('.q-field__control').contains('Тип задачи').click({ force: true})
    cy.get('.q-item__label').contains('Бронь').click({ force: true})
    cy.get('input.q-field__native.q-placeholder[aria-label="Поиск"][tabindex="0"][id^="f_"]').type('PMS №183')//найти нужную бронь
    cy.get('.text-right > .q-btn > .q-btn__wrapper').click()

    //cy.get('.q-linear-progress__model').should('not.be.visible') //проверяем, что наш лоадер исчез
    cy.get('tbody > :nth-child(1) > :nth-child(1)').should('be.visible') //проверяем отображание элемента в таблице задач
  })


  it('Загрузка файлов', () => {
    cy.get('tbody > .cursor-pointer > :nth-child(1)').click()
    cy.url().should('include', 'sanatop/private/tasks/1229')
    cy.get(':nth-child(4) > :nth-child(1) > :nth-child(1) > .q-btn__wrapper').click()
    const jpg = '4774350_0.jpg';
    cy.get('.q-uploader__input')
    cy.get('.q-uploader__input').attachFile(jpg)
    cy.get('.q-field__control-container').type('Gudetama')
    cy.get(':nth-child(3) > .q-btn__wrapper > .q-btn__content > .material-icons').click()
    cy.get('.q-mx-md > .q-btn > .q-btn__wrapper > .q-btn__content > .material-icons').click()
    cy.url().should('include', 'sanatop/private/tasks/1229')
    cy.get('.text-h6').should('contain', 'Бронь')

  })
})

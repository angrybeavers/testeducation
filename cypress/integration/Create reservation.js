describe('Create reservation', () => {

    before(function () {
        cy.testlogIn('admin@sanatop.ru', 'siUTyhbDgzS6f8yr')
    })

    beforeEach(function () {
        Cypress.Cookies.preserveOnce('idsrv.session')
    })


    it('Navigate to sanatorium tab', () => {
        cy.server()
        cy.route('GET', '/api/sanatoriums?PageNo=1&Size=20').as('sanatoriums')

        cy.get(':nth-child(6) > .q-btn__wrapper > .q-btn__content > .material-icons').click()
        cy.get('[href="/sanatop/admin/sanatoriums"]').click({
            force: true
        })

        cy.url().should('include', '/sanatop/admin/sanatoriums')
        cy.get('.text-h5').should('contain', 'Санатории')
        cy.wait('@sanatoriums').then((response) => {
            expect(response.status).to.eq(200)
        })
    })


    it('Авторизуемся напрямую через бекенд', () => {
        cy.request({
            url: 'https://sts.sanatop-test.quirco.com/connect/token',
            method: 'POST',
            form: true,
            body: {
                grant_type: 'password',
                username: 'admin@sanatop.ru',
                password: 'siUTyhbDgzS6f8yr',
                client_secret: 'r9WPw4cEcBSXNEbx',
                client_id: 'backend-dev'
            }
        }).then((response) => {
            expect(response.status).to.eq(200)
            cy.wrap(response.body.access_token).as('token')
        })
    })


    it('Create reservation', function () {
        const token = this.token
        cy.log(token)

        cy.request({
            url: 'https://sanatop-test.quirco.com/api/crs/reservation',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`//используем токен из предыдущего теста
            },
            form: false,
            body: {
                employee:null,
                inventoryBlockId:291,
                roomStays:[{
                    guests:[{
                        ratePlanId:2231,
                        ageQualifierId:null,
                        givenName:"Кот",
                        familyName:"Матроскин",
                        middleName:"Федорович"
                    }],
                    inventoryTypeId:660,
                    startDate:"2021-01-20",
                    endDate:"2021-01-28",
                    totalPriceRoomStay:108000,
                    comment:null}]
              }
        }).then((response) => {
            expect(response.status).to.eq(201)
        })
    }) 
 
      it('Создание компании', function () {
        const token = this.token
        cy.log(token)

        cy.request({
            url: '​/api/orders/companies',
            method: 'POST',
            headers: {
                Authorization: `Bearer ${token}`//используем токен из предыдущего теста
            },
            form: false,
            body: {
                    name: "Zombie",
                    shortName: "Zombie",
                    inn: "7743790915",
                    ogrn: "1107746688280",
                    kpp: "774301001",
                    checkingAccountNumber: "40702810640170004490",
                    correspondentAccountNumber: "30101810400000000225",
                    bic: "044525225",
                    bankName: "sber",
                    bankCity: "rostov",
                    amoCrmAccount: "string",
                    amoCrmUserName: "string",
                    amoCrmPassword: "string",
                    amoCrmClientId: "string",
                    amoCrmSecret: "string",
                    acquiringUserName: "string",
                    acquiringPassword: "string",
                    acquiringBaseUrl: "https://sanatorium-is.ru/",
                    isPaymentAgent: true
            }
        }).then((response) => {
            expect(response.status).to.eq(201)
        })
    }) 
})

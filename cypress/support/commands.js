// ***********************************************
// This example commands.js shows you how to
// create various custom commands and overwrite
// existing commands.
//
// For more comprehensive examples of custom
// commands please read more here:
// https://on.cypress.io/custom-commands
// ***********************************************
//
//
// -- This is a parent command --
// Cypress.Commands.add("login", (email, password) => { ... })
//
//
// -- This is a child command --
// Cypress.Commands.add("drag", { prevSubject: 'element'}, (subject, options) => { ... })
//
//
// -- This is a dual command --
// Cypress.Commands.add("dismiss", { prevSubject: 'optional'}, (subject, options) => { ... })
//
//
// -- This will overwrite an existing command --
// Cypress.Commands.overwrite("visit", (originalFn, url, options) => { ... })

Cypress.Commands.add('testlogIn', (email, password) => {
    cy.visit('https://sanatop-test.quirco.com')
    cy.get('#Username').type('admin@sanatop.ru')
    cy.get('#Password').type('siUTyhbDgzS6f8yr')
    cy.get('.col-sm-12 > .btn-primary').click()
})


Cypress.Commands.add('login', () => {
    cy.request({
        url: 'https://sts.sanatop-test.quirco.com/connect/token',
        method: 'POST',
        form: true,
        body: {
            grant_type: 'password',
            username: 'admin@sanatop.ru',
            password: 'siUTyhbDgzS6f8yr',
            client_secret: 'r9WPw4cEcBSXNEbx',
            client_id: 'backend-dev'
        }
    }).then((response) => {
        expect(response.status).to.eq(200)
        cy.wrap(response.body.access_token).as('token')
    })
})



Cypress.Commands.add('logIn', (email, password) => {
    cy.visit(Cypress.env('HOST'))
    cy.url().should('include', 'sts.sanatop')
    cy.get(Authorization.emailInput).focus().type(email)
    cy.get(Authorization.passwordInput).focus().type(pass)
    cy.get(Authorization.logInButton).click()
    cy.url().should('include', '/sanatop/private/orders')
});





  